<?php
   // require('animal.php');
   // require('frog.php');
   require('ape.php');

   $object = new Animal("shaun");

   echo "Name: ";
   echo $object -> name;
   echo "<br>";
   echo "Legs: ";
   echo $object -> legs;
   echo "<br>";
   echo "Cold Blooded: ";
   echo $object -> cold_blooded;


   $object2 = new Frog("buduk");

   echo "<br><br>Name: $object2->name <br>";
   echo "Legs: $object2->legs <br>";
   echo "Cold Blooded: $object2->cold_blooded <br>";
   echo "Jump: $object2->jump";

   $object3 = new Ape("kera sakti");

   echo "<br><br>Name: $object3->name <br>";
   echo "Legs: $object3->legs <br>";
   echo "Cold Blooded: $object3->cold_blooded <br>";
   echo "Yell: $object3->yell <br>";

?>