<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AdminLTE 3 | @yield('title')</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href={{ asset("assets/fontawesome-free/css/all.min.css") }}>
    <!-- Ionicons -->
    <link rel="stylesheet" href={{ asset("https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css") }}>
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href={{ asset("assets/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css") }}>
    <!-- iCheck -->
    <link rel="stylesheet" href={{ asset("assets/icheck-bootstrap/icheck-bootstrap.min.css") }}>
    <!-- JQVMap -->
    <link rel="stylesheet" href={{ asset("assets/jqvmap/jqvmap.min.css" )}}>
    <!-- Theme style -->
    <link rel="stylesheet" href={{ asset("assets/css/adminlte.min.css") }}>
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href={{ asset("assets/overlayScrollbars/css/OverlayScrollbars.min.css") }}>
    <!-- Daterange picker -->
    <link rel="stylesheet" href={{ asset("assets/daterangepicker/daterangepicker.css") }}>
    <!-- summernote -->
    <link rel="stylesheet" href={{ asset("assets/summernote/summernote-bs4.min.css") }}>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Preloader -->
        <div class="preloader flex-column justify-content-center align-items-center">
            <img class="animation__shake" src="{{ asset('assets/img/AdminLTELogo.png') }}" alt="AdminLTELogo" height="60" width="60">
        </div>

        <!-- Navbar -->
        @include('tugas13.layouts.partials.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('tugas13.layouts.partials.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">@yield('content2')</h1>
                        </div><!-- /.col -->
                        {{-- <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Dashboard v1</li>
                            </ol>
                        </div><!-- /.col --> --}}
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <!-- Small boxes (Stat box) -->
                    <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">@yield('content2')</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @yield('content')
                        </div>
                        <!-- /.card-body -->
                      </div>
                    <!-- /.row (main row) -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->


        <!-- Control Sidebar -->
        @include('tugas13.layouts.partials.footer')
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src={{ asset("assets/jquery/jquery.min.js") }}></script>
    <!-- jQuery UI 1.11.4 -->
    <script src={{ asset("assets/jquery-ui/jquery-ui.min.js") }}></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src={{ asset("assets/bootstrap/js/bootstrap.bundle.min.js") }}></script>
    <!-- ChartJS -->
    <script src={{ asset("assets/chart.js/Chart.min.js") }}></script>
    <!-- Sparkline -->
    <script src={{ asset("assets/sparklines/sparkline.js") }}></script>
    <!-- JQVMap -->
    <script src={{ asset("assets/jqvmap/jquery.vmap.min.js") }}></script>
    <script src={{ asset("assets/jqvmap/maps/jquery.vmap.usa.js") }}></script>
    <!-- jQuery Knob Chart -->
    <script src={{ asset("assets/jquery-knob/jquery.knob.min.js") }}></script>
    <!-- daterangepicker -->
    <script src={{ asset("assets/moment/moment.min.js") }}></script>
    <script src={{ asset("assets/daterangepicker/daterangepicker.js") }}></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src={{ asset("assets/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js") }}></script>
    <!-- Summernote -->
    <script src={{ asset("assets/summernote/summernote-bs4.min.js") }}></script>
    <!-- overlayScrollbars -->
    <script src={{ asset("assets/overlayScrollbars/js/jquery.overlayScrollbars.min.js") }}></script>
    <!-- AdminLTE App -->
    <script src={{ asset("assets/js/adminlte.js") }}></script>
    <!-- AdminLTE for demo purposes -->
    <script src={{ asset("assets/js/demo.js") }}></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src={{ asset("assets/js/pages/dashboard.js") }}></script>
    <script src={{ asset("assets/datatables/jquery.dataTables.js") }}></script>
    <script src={{ asset("assets/datatables-bs4/js/dataTables.bootstrap4.js") }}></script>
    <script>
        $(function () {
    $("#example1").DataTable();
  });
    </script>
</body>

</html>
