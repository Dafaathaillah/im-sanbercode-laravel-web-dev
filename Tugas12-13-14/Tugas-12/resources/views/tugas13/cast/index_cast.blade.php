@extends('tugas13.layouts.master')

@section('title')
Index Page
@endsection

@section('content2')
Index Page
@endsection

@section('content3')
IIndex
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary mb-3">Add New Data</a>
<table class="table">
   <thead class="thead-light">
      <tr>
         <th scope="col">No</th>
         <th scope="col">Name</th>
         <th scope="col">Umur</th>
         <th scope="col">Actions</th>
      </tr>
   </thead>
   <tbody>
      @forelse ($cast as $key=>$value)
      <tr>
         <td>{{$key + 1}}</th>
         <td>{{$value->name}}</td>
         <td>{{$value->umur}}</td>
         <td>
            <form action="/cast/{{$value->id}}" method="POST">
               @csrf
               @method('DELETE')
               <a href="/cast/{{$value->id}}" class="btn btn-success btn-sm">Show</a>
               <a href="/cast/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
               <input type="submit" class="btn btn-danger my-1 btn-sm" value="Delete">
            </form>
         </td>
      </tr>
      @empty
      <tr colspan="3">
         <td>No data</td>
      </tr>
      @endforelse
   </tbody>
</table>
@endsection