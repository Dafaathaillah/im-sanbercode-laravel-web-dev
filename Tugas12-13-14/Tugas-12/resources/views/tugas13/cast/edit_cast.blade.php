@extends('tugas13\layouts\master')

@section('title')
Edit Page
@endsection

@section('content2')
Edit Cast
@endsection

@section('content3')
Edit Data
@endsection

@section('content')
<form action="/cast/{{ $cast->id }}" method="POST">
   @csrf
   @method('PUT')
   <div class="row">
      <div class="form-group col-lg-6">
         <label for="name">Nama</label>
         <input type="text" class="form-control" value="{{ $cast->name }}" name="name" id="name" placeholder="Masukkan Nama">
         @error('name')
         <div class="alert alert-danger">
            {{ $message }}
         </div>
         @enderror
      </div>
      <div class="form-group col-lg-6">
         <label for="umur">Umur</label>
         <input type="number" class="form-control" value="{{ $cast->umur }}" name="umur" id="umur" placeholder="Masukkan Umur Anda">
         @error('umur')
         <div class="alert alert-danger">
            {{ $message }}
         </div>
         @enderror
      </div>
   </div>
   <div class="form-group">
      <label for="bio">Bio</label>
      {{-- <input type="text-area" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur Anda"> --}}
      <textarea name="bio" id="bio" class="form-control" placeholder="Tulis Bio Anda" cols="30" rows="10">{{ $cast->bio }}</textarea>
      @error('bio')
      <div class="alert alert-danger">
         {{ $message }}
      </div>
      @enderror
   </div>
   <button type="submit" class="btn btn-primary">Submit</button>
   <a href="/cast" class="btn btn-warning">cancel</a> 
</form>
@endsection